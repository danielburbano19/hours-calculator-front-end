import { browser, by, element, ElementFinder } from 'protractor';

export class ServiceReportPage {
  async navigateTo(): Promise<unknown> {
    return browser.get('http://localhost:4200/hours-calculator/inquiry-hours');
  }

  async getBtnSubmit(): Promise<ElementFinder> {
    return element(by.buttonText('Consultar'));
  }

  async getToast(): Promise<ElementFinder> {
    return element(by.css('#toast-container .toast-message'));
  }

  async getTechnicianId(): Promise<ElementFinder> {
    return element(by.name('technicianId'));
  }

  async getWeekNumber(): Promise<ElementFinder> {
    return element(by.name('weekNumber'));
  }

  async getTable(): Promise<ElementFinder> {
    return element(by.name('dataTable'));
  }
}
