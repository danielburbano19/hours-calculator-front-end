import { browser, element, by, logging } from 'protractor';
import { ServiceReportPage } from './hours-calculator.po';

describe('workspace-project App', () => {
  let page: ServiceReportPage;

  beforeEach(() => {
    page = new ServiceReportPage();
  });

  it('should display a message with error when hours calculator form data is incorrectly filled out', async () => {
    await page.navigateTo();
    const btnSubmit = await page.getBtnSubmit();
    await btnSubmit.click().then(async () => {
      const toast = await page.getToast();
      browser.wait(() => {
        return toast.isPresent();
      }, 10000);
      const toastMessage = await toast.getText();
      expect(toastMessage).toBe('Verifique los datos del formulario');
    });
  });

  it('should display a table with calculated hours when form data is correctly filled out', async () => {    
    const technicianId = await page.getTechnicianId();
    await technicianId.sendKeys('500');
    const weekNumber = await page.getWeekNumber();
    await weekNumber.sendKeys('1');
    const btnSubmit = await page.getBtnSubmit();
    await btnSubmit.click().then(async () => {
      const toast = await page.getToast();
      browser.wait(() => {
        return toast.isPresent();
      }, 10000);
    });
    const table = await page.getTable();    
    const rowsNumber = await table.$$('tr').count();
    expect(rowsNumber).toBe(7);
  });

  afterEach(async () => {
    const logs = await browser.manage().logs().get(logging.Type.BROWSER);
    for (const log of logs) {
      if (log.message.includes('net::ERR_CONNECTION_REFUSED')) {
        const toast = await page.getToast();
        const toastMessage = await toast.getText();
        expect(toastMessage).toBe('Ocurrió un error al consultar');
      }
    }
  });
});
