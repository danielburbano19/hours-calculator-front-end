import { browser, element, by, logging } from 'protractor';
import { ServiceReportPage } from './service-report.po';

describe('workspace-project App', () => {
  let page: ServiceReportPage;

  beforeEach(() => {
    page = new ServiceReportPage();
  });

  it('should display a message with error when service report form data is incorrectly filled out', async () => {
    browser.wait(() => {}, 20000);
    await page.navigateTo();
    const btnSubmit = await page.getBtnSubmit();
    await btnSubmit.click().then(async () => {
      const toast = await page.getToast();
      browser.wait(() => {
        return toast.isPresent();
      }, 10000);
      const toastMessage = await toast.getText();
      expect(toastMessage).toBe('Verifique los datos del formulario');
    });
  });

  it('should display a message with success when service report is correctly created', async () => {
    await element(by.name('technicianId')).sendKeys('500');
    await element(by.name('serviceId')).sendKeys('123');
    const startDate = await page.getStartDate();
    await startDate.click().then(() => {
      startDate.sendKeys('2021-04-18');
    });
    const startTime = await page.getStartTime();
    await startTime.click().then(async () => {
      await element(by.cssContainingText('span', '10')).click();
      await element(by.buttonText('Ok')).click();
    });
    const endDate = await page.getEndDate();
    await endDate.click().then(() => {
      endDate.sendKeys('2021-04-18');
    });
    const endTime = await page.getEndTime();
    await endTime.click().then(async () => {
      await element(by.cssContainingText('span', '11')).click();
      await element(by.buttonText('Ok')).click();
    });
    const btnSubmit = await page.getBtnSubmit();
    await btnSubmit.click().then(async () => {
      const toast = await page.getToast();
      browser.wait(() => {
        return toast.isPresent();
      }, 10000);
      const toastMessage = await toast.getText();
      expect(toastMessage).toBe(
        'El registro para el técnico 500 se registró exitosamente'
      );
    });
  });

  afterEach(async () => {
    const logs = await browser.manage().logs().get(logging.Type.BROWSER);
    for (const log of logs) {
      if (log.message.includes('net::ERR_CONNECTION_REFUSED')) {
        const toast = await page.getToast();
        const toastMessage = await toast.getText();
        expect(toastMessage).toBe('Error al registrar');
      }
    }
  });
});
