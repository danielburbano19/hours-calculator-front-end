import { browser, by, element, ElementFinder } from 'protractor';

export class ServiceReportPage {
  async navigateTo(): Promise<unknown> {
    return browser.get('http://localhost:4200/service-report/registration-form');
  }

  async getBtnSubmit(): Promise<ElementFinder> {
    return element(by.buttonText('Registrar'));
  }

  async getToast(): Promise<ElementFinder> {
    return element(by.css('#toast-container .toast-message'));
  }

  async getTechnicianId(): Promise<ElementFinder> {
    return element(by.name('technicianId'));
  }

  async getServiceId(): Promise<ElementFinder> {
    return element(by.name('serviceId'));
  }

  async getStartDate(): Promise<ElementFinder> {
    return element(by.name('startDate'));
  }

  async getStartTime(): Promise<ElementFinder> {
    return element(by.name('startTime'));
  }

  async getEndDate(): Promise<ElementFinder> {
    return element(by.name('endDate'));
  }

  async getEndTime(): Promise<ElementFinder> {
    return element(by.name('endTime'));
  }
}
