import { Component, OnInit } from '@angular/core';
import {
  FormBuilder,
  FormGroup,
  Validators,
  FormGroupDirective,
} from '@angular/forms';
import * as moment from 'moment';
import { ToastrsService } from 'src/app/shared/services/toastrs.service';
import { ServiceReportService } from '../../services/service-report.service';
import { ServiceReportModel } from '../../types';
moment.locale('es');

@Component({
  selector: 'app-registration-form',
  templateUrl: './registration-form.component.html',
  styleUrls: ['./registration-form.component.scss'],
})
export class RegistrationFormComponent implements OnInit {
  form: FormGroup;
  constructor(
    private formBuilder: FormBuilder,
    private serviceReport: ServiceReportService,
    private toastr: ToastrsService
  ) {}

  ngOnInit(): void {
    this.createForm();
  }

  private createForm() {
    this.form = this.formBuilder.group({
      technicianId: ['', Validators.required],
      serviceId: ['', Validators.required],
      startDate: ['', Validators.required],
      startTime: ['', Validators.required],
      endDate: ['', Validators.required],
      endTime: ['', Validators.required],
    });
  }

  public fieldNotValid(fieldName: string): boolean {
    return (
      this.form.controls[fieldName].invalid &&
      this.form.controls[fieldName].touched
    );
  }

  onSubmit(formDirective: FormGroupDirective) {
    if (this.form.invalid) {
      this.toastr.error('Verifique los datos del formulario');
      return Object.values(this.form.controls).forEach((control) => {
        control.markAsTouched();
      });
    }
    const technicianId = this.form.value.technicianId;
    const serviceId = this.form.value.serviceId;
    const startTime = this.formatDatetime(
      this.form.value.startDate,
      this.form.value.startTime
    );
    const endTime = this.formatDatetime(
      this.form.value.endDate,
      this.form.value.endTime
    );
    const body: ServiceReportModel = {
      technicianId,
      serviceId,
      startTime,
      endTime,
    };
    this.serviceReport.createServiceReport(body).subscribe(
      (res: ServiceReportModel) => {
        formDirective.resetForm();
        this.form.reset();        
        this.toastr.success(
          `El registro para el técnico ${res.technicianId} se registró exitosamente`
        );
      },
      (error: any) => {
        if (
          error?.error?.message ===
          'startTime could not be greater or equal than endTime'
        ) {
          this.toastr.error(
            'Error al registrar, la fecha y hora inicial no puede ser mayor o igual a la fecha y hora final'
          );
        } else if (
          error?.error?.message ===
          'Range in startTime and endTime could not be greater or equal than 12'
        ) {
          this.toastr.error(
            'Error al registrar, el rango entre la fecha inicial y la fecha final debe ser máximo de 12 horas'
          );
        } else {
          this.toastr.error('Error al registrar');
        }
      }
    );
  }

  private formatDatetime(date: Date, time: string): string {
    const dateFormated = moment(date).format('YYYY-MM-DD');
    return `${dateFormated}T${time}`;
  }
}
