interface ServiceReportModel {
  _id?: String;
  technicianId: string;
  serviceId: string;
  startTime: string;
  endTime: string;
}

export { ServiceReportModel };
