import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RegistrationFormComponent } from './component/registration-form/registration-form.component';

const routes: Routes = [
  {
    path: '',    
    children: [
      {
        path: 'registration-form',
        component: RegistrationFormComponent
      },      
      {
        path: '**',
        redirectTo: 'registration-form',
        pathMatch: 'full',
      }
    ]
  }
];

@NgModule({
  declarations: [],
  imports: [
    RouterModule.forChild(routes)
  ],
  exports:[
    RouterModule
  ]
})
export class ServiceReportRoutingModule { }
