import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RegistrationFormComponent } from './component/registration-form/registration-form.component';
import { ServiceReportComponent } from './service-report.component';
import { ServiceReportRoutingModule } from './service-report-routing.module';
import { AngularMaterialModule } from '../shared/angular-material/angular-material.module';
import { NgxMaterialTimepickerModule } from 'ngx-material-timepicker';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [RegistrationFormComponent, ServiceReportComponent],
  imports: [
    CommonModule,
    ServiceReportRoutingModule,
    AngularMaterialModule,
    NgxMaterialTimepickerModule,
    FormsModule,
    ReactiveFormsModule
  ],
})
export class ServiceReportModule {}
