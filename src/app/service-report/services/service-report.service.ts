import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { pipe } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class ServiceReportService {
  private _url = environment.url;
  private _paths: any = {
    serviceReport: '/service-report',
  };
  constructor(private http: HttpClient) {}

  createServiceReport(data) {
    const httpOptions = {
      headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
    };

    return this.http.post(
      `${this._url}${this._paths.serviceReport}`,
      data,
      httpOptions
    );
  }
}
