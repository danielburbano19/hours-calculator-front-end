import {
  HttpClientTestingModule,
  HttpTestingController,
} from '@angular/common/http/testing';
import { HttpResponse } from '@angular/common/http';
import { TestBed } from '@angular/core/testing';

import { ServiceReportService } from './service-report.service';
import { ServiceReportModel } from '../types';
import { environment } from 'src/environments/environment';

describe('ServiceReportService', () => {
  let httpTestingController: HttpTestingController;
  let service: ServiceReportService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [ServiceReportService],
    });
    httpTestingController = TestBed.inject(HttpTestingController);
    service = TestBed.inject(ServiceReportService);
  });

  it('should add a service report and return it', () => {
    const newServiceReport: ServiceReportModel = {
      _id: '1',
      technicianId: '900',
      serviceId: '123',
      startTime: '2021-04-18T07:00:00',
      endTime: '2021-04-18T08:00:00',
    };

    service
      .createServiceReport(newServiceReport)
      .subscribe((data) => expect(data).toEqual(newServiceReport), fail);

    const req = httpTestingController.expectOne(
      `${environment.url}/service-report`
    );
    expect(req.request.method).toEqual('POST');
    expect(req.request.headers.get('Content-Type')).toEqual('application/json');
    expect(req.request.body).toEqual(newServiceReport);

    const expectedResponse = new HttpResponse({
      status: 201,
      statusText: 'Created',
      body: newServiceReport,
    });
    req.event(expectedResponse);
  });

  it('should return an error when the server returns a 400', () => {
    const newServiceReport = {};
    const errorResponse = new HttpResponse({
      status: 400,
      statusText: 'Bad Request',
    });
    service
      .createServiceReport(newServiceReport)
      .subscribe((data) => console.log(data), fail);
    const req = httpTestingController.expectOne(
      `${environment.url}/service-report`
    );
    expect(req.request.method).toEqual('POST');
    expect(req.request.headers.get('Content-Type')).toEqual('application/json');
    req.event(errorResponse);
  });

  afterEach(() => {
    httpTestingController.verify();
  });
});
