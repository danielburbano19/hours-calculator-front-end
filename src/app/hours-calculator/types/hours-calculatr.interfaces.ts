interface HoursResponse {
  normalHours: number;
  sundayHours: number;
  extraNormalHours: number;
  nightHours: number;
  extraSundayHours: number;
  extraNightHours: number;
}

interface HourData {
  hourConcept: string;
  hoursNumber: number;
}

interface HourRequest {
  technicianId: string;
  weekNumber: string;
}

interface HourDisplay {
  label: string;
  value: string;
}

export { HoursResponse, HourData, HourRequest, HourDisplay };
