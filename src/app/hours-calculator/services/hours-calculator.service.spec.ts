import { HttpResponse } from '@angular/common/http';
import {
  HttpClientTestingModule,
  HttpTestingController,
} from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { environment } from 'src/environments/environment';
import { HourData, HourRequest, HoursResponse } from '../types';

import { HoursCalculatorService } from './hours-calculator.service';

describe('HoursCalculatorService', () => {
  let httpTestingController: HttpTestingController;
  let service: HoursCalculatorService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [HoursCalculatorService],
    });
    
    httpTestingController = TestBed.inject(HttpTestingController);
    service = TestBed.inject(HoursCalculatorService);
  });

  it('should return hours calculated for technician and week number', () => {
    const hoursResponseMock: HoursResponse = {
      normalHours: 1,
      sundayHours: 2,
      extraNormalHours: 13,
      nightHours: 0,
      extraSundayHours: 0,
      extraNightHours: 0,
    };

    const formatedHoursResponseMock:HourData[] =[
      { hourConcept: 'Horas normales', hoursNumber: 1 },
      { hourConcept: 'Horas dominicales', hoursNumber: 2 },
      { hourConcept: 'Horas extras normales', hoursNumber: 13 },
      { hourConcept: 'Horas nocturnas', hoursNumber: 0 },
      { hourConcept: 'Horas extras dominicales', hoursNumber: 0 },
      { hourConcept: 'Horas extras nocturnas', hoursNumber: 0 },
    ];

    const hoursRequest: HourRequest = {
      technicianId: '700',
      weekNumber: '12',
    };

    service.getHoursCalculated(hoursRequest).subscribe((data) => {
      expect(data).toEqual(formatedHoursResponseMock), fail;
    });
    const req = httpTestingController.expectOne(
      `${environment.url}/hours-calculator/700/week/12`
    );

    expect(req.request.method).toBe('GET');
    req.flush(hoursResponseMock);    
  });

  it('should return an error when the server returns a 500', () => {
    const hoursRequest: HourRequest = {
      technicianId: '700',
      weekNumber: '12',
    };
    const errorResponse = new HttpResponse({
      status: 500,
      statusText: 'Internal Server Error',
    });
    service
      .getHoursCalculated(hoursRequest)
      .subscribe((data) => console.log(data), fail);
    const req = httpTestingController.expectOne(
      `${environment.url}/hours-calculator/700/week/12`
    );
    expect(req.request.method).toBe('GET');   
    req.event(errorResponse);
  });

  afterEach(() => {
    httpTestingController.verify();
  });
});
