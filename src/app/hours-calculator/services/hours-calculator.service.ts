import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators';
import { HourData, HourRequest } from '../types';

@Injectable({
  providedIn: 'root',
})
export class HoursCalculatorService {
  private _url = environment.url;
  private _paths: any = {
    hoursCalculator: '/hours-calculator',
  };
  private hourConceptTranslates = {
    normalHours: 'Horas normales',
    nightHours: 'Horas nocturnas',
    sundayHours: 'Horas dominicales',
    extraNormalHours: 'Horas extras normales',
    extraNightHours: 'Horas extras nocturnas',
    extraSundayHours: 'Horas extras dominicales',
  };
  constructor(private http: HttpClient) {}

  getHoursCalculated(hourRequest: HourRequest) {
    const { technicianId, weekNumber } = hourRequest;
    return this.http
      .get(
        `${this._url}${this._paths.hoursCalculator}/${technicianId}/week/${weekNumber}`
      )
      .pipe(
        map((item: any) => {
          if (item) {
            const hourDataList: HourData[] = [];
            const keys = Object.keys(item);
            for (const key of keys) {
              this.getLabelsHourConcept('');
              hourDataList.push({
                hourConcept: this.getLabelsHourConcept(key),
                hoursNumber: item[`${key}`],
              });
            }
            return hourDataList;
          }
        })
      );
  }

  getLabelsHourConcept(key) {
    const keys = Object.keys(this.hourConceptTranslates);
    for (const hourTranslate of keys) {
      if (key == hourTranslate)
        return this.hourConceptTranslates[`${hourTranslate}`];
    }
  }
}
