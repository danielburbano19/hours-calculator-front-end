import { Component, OnInit } from '@angular/core';
import { HourData } from './types';

@Component({
  selector: 'app-hours-calculator',
  templateUrl: './hours-calculator.component.html',
  styleUrls: ['./hours-calculator.component.scss'],
})
export class HoursCalculatorComponent implements OnInit {
  hourDataList: HourData[] = [];

  constructor() {}

  ngOnInit(): void {}

  mapHourData(data: HourData[]) {
    this.hourDataList = data;
  }
}
