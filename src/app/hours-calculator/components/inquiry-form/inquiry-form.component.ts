import {
  Component,
  EventEmitter,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSort } from '@angular/material/sort';
import { ToastrsService } from 'src/app/shared/services/toastrs.service';
import { HoursCalculatorService } from '../../services/hours-calculator.service';
import { HourData, HourRequest, HoursResponse } from '../../types';

@Component({
  selector: 'inquiry-form',
  templateUrl: './inquiry-form.component.html',
  styleUrls: ['./inquiry-form.component.scss'],
})
export class InquiryFormComponent implements OnInit {
  form: FormGroup;
  @ViewChild(MatSort) sort: MatSort;
  @Output() mapDataHour: EventEmitter<HourData[]> = new EventEmitter<
    HourData[]
  >();
  constructor(
    private toastr: ToastrsService,
    private formBuilder: FormBuilder,
    private hoursCalculatorService: HoursCalculatorService
  ) {}

  ngOnInit(): void {
    this.createForm();
  }

  private createForm() {
    this.form = this.formBuilder.group({
      technicianId: ['', Validators.required],
      weekNumber: ['', Validators.required],
    });
  }

  public fieldNotValid(fieldName: string): boolean {
    return (
      this.form.controls[fieldName].invalid &&
      this.form.controls[fieldName].touched
    );
  }

  onSubmit() {
    if (this.form.invalid) {
      this.toastr.error('Verifique los datos del formulario');
      return Object.values(this.form.controls).forEach((control) => {
        control.markAsTouched();
      });
    }
    const technicianId = this.form.value.technicianId;
    const weekNumber = this.form.value.weekNumber;
    this.getHourCalculatedList(technicianId, weekNumber);
  }

  getHourCalculatedList(technicianId: string, weekNumber: string) {
    const request: HourRequest = {
      technicianId,
      weekNumber,
    };
    this.hoursCalculatorService.getHoursCalculated(request).subscribe(
      (response: any) => this.mapDataHour.emit(response),
      (error: any) => {
      console.log("🚀 ~ file: inquiry-form.component.ts ~ line 69 ~ InquiryFormComponent ~ getHourCalculatedList ~ error", error)
      this.toastr.error('Ocurrió un error al consultar')
      }
    );
  }
}
