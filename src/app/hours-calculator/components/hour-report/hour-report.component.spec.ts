import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HourReportComponent } from './hour-report.component';

describe('HourReportComponent', () => {
  let component: HourReportComponent;
  let fixture: ComponentFixture<HourReportComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [HourReportComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HourReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });
});
