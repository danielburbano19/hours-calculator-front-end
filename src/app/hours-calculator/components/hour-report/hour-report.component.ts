import {
  AfterViewInit,
  Component,
  Input,
  OnChanges,
  ViewChild,
} from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { HourData, HourDisplay } from '../../types';
import { MatSort } from '@angular/material/sort';

@Component({
  selector: 'hour-report',
  templateUrl: './hour-report.component.html',
  styleUrls: ['./hour-report.component.scss'],
})
export class HourReportComponent implements OnChanges, AfterViewInit {
  @Input() data: HourData[] = [];
  hourDisplayList: HourDisplay[] = [
    { label: 'Concepto', value: 'hourConcept' },
    { label: 'Cantidad de horas', value: 'hoursNumber' },
  ];
  dataSource: MatTableDataSource<HourData>;
  @ViewChild(MatSort) matSort: MatSort;

  ngAfterViewInit(): void {
    this.implementsDatatable();
  }

  ngOnChanges(): void {
    this.implementsDatatable();
  }

  get displayedColumns(): string[] {
    return this.hourDisplayList.map((item) => item.value);
  }

  isDataEmpty() {
    return this.data.length ? false : true;
  }

  implementsDatatable() {
    this.dataSource = new MatTableDataSource(this.data);
    this.dataSource.sort = this.matSort;
  }
}
