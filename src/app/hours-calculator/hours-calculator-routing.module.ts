import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HoursCalculatorComponent } from './hours-calculator.component';

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: 'inquiry-hours',
        component: HoursCalculatorComponent,
      },
      {
        path: '**',
        redirectTo: 'inquiry-hours',
      },
    ],
  },
];

@NgModule({
  declarations: [],
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class HoursCalculatorRoutingModule {}
