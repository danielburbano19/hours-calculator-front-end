import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HoursCalculatorComponent } from './hours-calculator.component';
import { InquiryFormComponent } from './components/inquiry-form/inquiry-form.component';
import { HourReportComponent } from './components/hour-report/hour-report.component';
import { HoursCalculatorRoutingModule } from './hours-calculator-routing.module';
import { AngularMaterialModule } from '../shared/angular-material/angular-material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NumbersOnlyDirective } from '../shared/directives/numbers-only.directive';

@NgModule({
  declarations: [
    HoursCalculatorComponent,
    InquiryFormComponent,
    HourReportComponent,
    NumbersOnlyDirective,
  ],
  imports: [
    CommonModule,
    HoursCalculatorRoutingModule,
    AngularMaterialModule,
    FormsModule,
    ReactiveFormsModule,
  ],
})
export class HoursCalculatorModule {}
