import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'hours-calculator',
    loadChildren: () =>
      import('./hours-calculator/hours-calculator.module').then(
        (m) => m.HoursCalculatorModule
      ),
  },
  {
    path: 'service-report',
    loadChildren: () =>
      import('./service-report/service-report.module').then(
        (m) => m.ServiceReportModule
      ),
  },
  {
    path: '**',
    redirectTo: 'service-report',
    pathMatch: 'full',
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
