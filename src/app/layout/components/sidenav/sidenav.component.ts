import { MediaMatcher } from '@angular/cdk/layout';
import {  Component } from '@angular/core';

@Component({
  selector: 'app-sidenav',
  templateUrl: './sidenav.component.html',
  styleUrls: ['./sidenav.component.scss'],
})
export class SidenavComponent {
  mobileQuery: MediaQueryList;  
  fillerNav = [
    {
      label: 'Registrar servicio',
      path: '/service-report/registration-form',
      icon: 'wysiwyg',
    },
    {
      label: 'Calculadora de horas',
      path: '/hours-calculator/inquiry-hours',
      icon: 'calculate',
    },
  ];
  
  constructor(    
    media: MediaMatcher,    
  ) {
    this.mobileQuery = media.matchMedia('(max-width: 600px)');    
  }
}
